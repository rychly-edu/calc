var db = null;

function openDb() {
	var Database_Name = 'calc';
	var Version = 1.0;
	var Text_Description = '';
	var Database_Size = 2 * 1024 * 1024;
	db = openDatabase(Database_Name, Version, Text_Description, Database_Size);
}

function initDb() {
	// DDL
	db.transaction(function (tx) {
		tx.executeSql(`
		CREATE TABLE IF NOT EXISTS task (
			spec TEXT PRIMARY KEY,
			result REAL NOT NULL,
			random INTEGER NOT NULL
		)`, [], onSuccessExecuteSql, onError);
		tx.executeSql(`
		CREATE TABLE IF NOT EXISTS try (
			timestamp INTEGER,
			spec TEXT,
			seconds INTEGER NOT NULL,
			success INTEGER,
			PRIMARY KEY (spec, timestamp),
			FOREIGN KEY(spec) REFERENCES task(spec)
		)`, [], onSuccessExecuteSql, onError);
		tx.executeSql(`
		CREATE INDEX IF NOT EXISTS try_spec ON try(spec)
		`, [], onSuccessExecuteSql, onError);
	}, onError, onReadyTransaction);
}

function initData() {
	// DML for all possible tasks
	db.transaction(function (tx) {
		for (let n1 = 0; n1 <= 10; n1++) {
			for (let n2 = 0; n2 <= 10; n2++) {
				let result = n1 * n2;
				tx.executeSql(`
				INSERT OR IGNORE INTO task (spec, result, random) VALUES (?, ?, ?)
				`, [`${n1} ⋅ ${n2}`, result, Math.floor(Math.random() * 255)], onSuccessExecuteSql, onError);
				if (n1 != 0) {
					tx.executeSql(`
					INSERT OR IGNORE INTO task (spec, result, random) VALUES (?, ?, ?)
					`, [`${result} : ${n1}`, n2, Math.floor(Math.random() * 255)], onSuccessExecuteSql, onError);
				}
				if (n2 != 0) {
					tx.executeSql(`
					INSERT OR IGNORE INTO task (spec, result, random) VALUES (?, ?, ?)
					`, [`${result} : ${n2}`, n1, Math.floor(Math.random() * 255)], onSuccessExecuteSql, onError);
				}
			}
		}
	}, onError, onReadyTransaction);
}

function onReadyTransaction() {
	console.info('Transaction completed');
}

function onSuccessExecuteSql(tx, results) {
	console.info('Execute SQL completed');
}

function onError(err) {
	console.error(err);
}

function resultsToTable(table, tx, results) {
	if (results.rows.length == 0) return false;
	// table headers
	let columns = Object.keys(results.rows.item(0));
	let tableRow = document.createElement('tr');
	for (let column of columns) {
		let cell = document.createElement('th');
		cell.appendChild(document.createTextNode(column));
		tableRow.appendChild(cell);
	}
	table.appendChild(tableRow);
	// table rows
	for (let row of results.rows) {
		let tableRow = document.createElement('tr');
		for (let column of columns) {
			let cell = document.createElement('td');
			cell.appendChild(document.createTextNode(row[column]));
			tableRow.appendChild(cell);
		}
		table.appendChild(tableRow);
	}
	table.style.pointerEvents = 'none';
	return true;
}

function leastSuccessToday(tableElement, limit) {
	db.transaction(function (tx) {
		tx.executeSql(`
		SELECT spec, sum(success) AS successes, count(*) AS tries, avg(seconds) AS seconds, datetime(max(timestamp), 'unixepoch', 'localtime') AS last
		FROM task NATURAL JOIN try
		WHERE date(datetime(timestamp, 'unixepoch')) = date('now')
		GROUP BY spec
		ORDER BY successes ASC, seconds DESC, last DESC
		LIMIT ?
		`, [limit], (tx, results) => {
			return resultsToTable(tableElement, tx, results);
		}, onError);
	}, onError, onReadyTransaction);
}

function leastSuccessEver(tableElement, limit) {
	db.transaction(function (tx) {
		tx.executeSql(`
		SELECT spec, sum(success) AS successes, count(*) AS tries, avg(seconds) AS seconds, datetime(max(timestamp), 'unixepoch', 'localtime') AS last
		FROM task NATURAL JOIN try
		GROUP BY spec
		ORDER BY successes ASC, seconds DESC, last DESC
		LIMIT ?
		`, [limit], (tx, results) => {
			return resultsToTable(tableElement, tx, results);
		}, onError);
	}, onError, onReadyTransaction);
}

function newTask(callback) {
	function displayTask(tx, results) {
		if (results.rows.length == 0) {
			console.error("No task to print");
			return false;
		}
		startTime = new Date();
		callback(results.rows.item(0).spec);
		return true;
	}
	db.transaction(function (tx) {
		tx.executeSql(`
		SELECT spec
		FROM task LEFT OUTER NATURAL JOIN try
		GROUP BY spec
		ORDER BY sum(success) ASC, sum(seconds) DESC, random
		LIMIT 1
		`, [], displayTask, onError);
	}, onError, onReadyTransaction);
}

function checkResult(spec, result, callback) {
	if (startTime == null) {
		console.error("Checking result without printing a task previously");
		return false;
	}
	let timestamp = Math.round(startTime.getTime() / 1000);
	let seconds = Math.round(((new Date()) - startTime) / 1000);
	db.transaction(function (tx) {
		function processResult(tx, results) {
			tx.executeSql(`
			INSERT OR IGNORE INTO try (timestamp, spec, seconds, success) VALUES (?, ?, ?, ?)
			`, [timestamp, spec, seconds, results.rows.length], onSuccessExecuteSql, onError);
			startTime = null;
			callback(results.rows.length != 0, seconds);
			return true;
		}
		tx.executeSql(`
		SELECT 1 FROM task WHERE spec = ? AND result = ?
		`, [spec, result], processResult, onError);
	}, onError, onReadyTransaction);
}

function secondsToMinutes(seconds) {
	let min = Math.floor(seconds / 60);
	let sec = seconds - min * 60;
	return min + ':' + (sec < 10 ? '0' : '') + sec;
}

var openTime = new Date();
var startTime = null;
var calcTimeSec = 0;
var successCounter = 0;
var failureCounter = 0;

const checkLabel = '✅ / ❌';
const successLabel = '✅ ▶';
const failLabel = '❌ ▶';

const resultId = 'result';
const submitId = 'submit';
const taskId = 'task';
const successesId = 'successes';
const failuresId = 'failures';
const countId = 'count';
const countSuccessRatio = 'countSuccessRatio';
const timeTotalId = 'totalTime';
const timeCalcId = 'calculatingTime';
const timeLastId = 'lastTime';
const failListId = 'failList';
const calcTotalTimeRatioId = 'calcTotalTimeRatio';
const successClass = 'success';
const failureClass = 'failure';

function checkOrPrintTask() {
	if (startTime == null) {
		return printTask();
	} else {
		return checkTask();
	}
}

function printTask() {
	with (document.getElementById(resultId)) {
		value = null;
		readOnly = false;
		focus();
	}
	document.documentElement.className = null;
	document.getElementById(submitId).innerText = checkLabel;
	return newTask((task) => {
		document.getElementById(taskId).innerText = task;
	});
}

function checkTask() {
	let spec = document.getElementById(taskId).innerText;
	let result = document.getElementById(resultId).value.trim();
	document.getElementById(resultId).value = result;
	if (result == '') return false; // do not allow an empty result
	document.getElementById(resultId).readOnly = true;
	return checkResult(spec, result, (success, seconds) => {
		if (success) {
			successCounter++;
			document.documentElement.className = successClass;
			document.getElementById(submitId).innerText = successLabel;
		} else {
			failureCounter++;
			document.documentElement.className = failureClass;
			document.getElementById(submitId).innerText = failLabel;
			let listItem = document.createElement('li');
			listItem.appendChild(document.createTextNode(`${spec} [${seconds} sec]`));
			document.getElementById(failListId).appendChild(listItem);
		}
		calcTimeSec += seconds;
		let totalTimeSec = Math.round(((new Date()) - openTime) / 1000);
		document.getElementById(countId).innerText = successCounter + failureCounter;
		document.getElementById(successesId).innerText = successCounter;
		document.getElementById(failuresId).innerText = failureCounter;
		document.getElementById(countSuccessRatio).innerText = Math.round(successCounter / (successCounter + failureCounter) * 100);
		document.getElementById(timeLastId).innerText = secondsToMinutes(seconds);
		document.getElementById(timeTotalId).innerText = secondsToMinutes(totalTimeSec);
		document.getElementById(timeCalcId).innerText = secondsToMinutes(calcTimeSec);
		document.getElementById(calcTotalTimeRatioId).innerText = Math.round(calcTimeSec / totalTimeSec * 100);
	});
}